%define debug_package %{nil}

Name:           rear
Version:        2.7
Release:        5
License:        GPLv3
Summary:        Relax-and-Recover is a setup-and-forget Linux bare metal disaster recovery solution
URL:            http://relax-and-recover.org/
Source0:        https://github.com/rear/rear/archive/refs/tags/%{version}.tar.gz
# Add cronjob and systemd timer as documentation
Source1:        rear.cron
Source2:        rear.service
Source3:        rear.timer
Patch0:         CVE-2024-23301.patch
ExclusiveArch:  x86_64 loongarch64 ppc64le sw_64 riscv64
Requires:       binutils ethtool gzip iputils parted tar openssl gawk attr bc crontabs iproute
Requires:       genisoimage util-linux
%ifarch x86_64 i686
Requires:       syslinux
%endif
BuildRequires:  make asciidoctor
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

%description
Relax-and-Recover is a setup-and-forget Linux bare metal disaster recovery solution. It is easy to
set up and requires no maintenance so there is no excuse for not using it.

%package        help
Summary:        Help document for the rear
Buildarch:      noarch

%description    help
Help document for the rear package.

%pre
if [ $1 -gt 1 ] ; then
  rm -rf %{_datadir}/rear/output/NETFS
fi

%prep
%autosetup -p1

sed -E -e "s:([\"' ])/lib:\1/usr/lib:g" \
    -i usr/share/rear/prep/GNU/Linux/*include*.sh

sed -e 's:/lib/:/usr/lib/:g' \
    -e 's:/lib\*/:/usr/lib\*/:g' \
    -e 's:/usr/usr/lib:/usr/lib:g' \
    -i 'usr/share/rear/conf/GNU/Linux.conf'

%build
make doc

%install
%make_install
install -p -d %{buildroot}%{_docdir}/%{name}/
install -m 0644 %{SOURCE1} %{buildroot}%{_docdir}/%{name}/
install -m 0644 %{SOURCE2} %{buildroot}%{_docdir}/%{name}/
install -m 0644 %{SOURCE3} %{buildroot}%{_docdir}/%{name}/

%clean
rm -rf %{buildroot}

%files
%doc MAINTAINERS COPYING README.adoc doc/*.txt doc/user-guide/*.html
%config(noreplace) %{_sysconfdir}/rear/
%doc %{_docdir}/%{name}/rear.*
%{_datadir}/rear/
%{_sharedstatedir}/rear/
%{_sbindir}/rear

%files help
%doc %{_mandir}/man8/rear.8*

%changelog
* Wed Mar 12 2025 Dingli Zhang <dingli@iscas.ac.cn> - 2.7-5
- Add riscv64 support

* Fri Feb 28 2025 zhangshaoning <zhangshaoning@uniontech.com> - 2.7-4
- Add sw_64 support

* Tue Mar 5 2024 yinxiulin <xiulin.yin@shingroup.cn> - 2.7-3
- Add ppc64le support

* Mon Jan 15 2024 wangkai <13474090681@163.com> - 2.7-2
- Fix CVE-2024-23301

* Thu Jan 04 2024 Paul Thomas <paulthomas100199@gmail.com> - 2.7-1
- update to version 2.7

* Tue Oct 17 2023 yaoxin <yao_xin001@hoperun.com> - 2.6-1
- Upgrade to 2.6

* Tue Nov 15 2022 huajingyun <huajingyun@loongson.cn> - 2.4-4
- Add loongarch64 support

* Wed Feb 26 2020 Ling Yang <lingyang2@huawei.com> - 2.4-3
- Package Init
